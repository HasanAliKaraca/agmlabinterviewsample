﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DOMAIN;

namespace DAL
{
    /*
     * Not: Normalde  map işlemi manuel yapılmaz, automapper kullanılması lazım ancak database first yaklaşımından
     * kullanıldığı için model1.tt dosyasının oluşturduğu classlar domain projesi içinde kalmıştır.
     * Proje mimarisi değiştirilmesi istenmediği için de automapper kullanamadım. 
     * Entity objectten dto'ya map işlemini bu yüzden ben de manuel gerçekleştirdim.
     *
     */

    public class TaskManager : BaseManager
    {

        /// <summary>
        /// Spesifik Id'li UserTask'i getirir
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UserTaskDto GetTask(int id)
        {
            var currentUserTask = new UserTaskDto();
            try
            {
                using (entity = new InterviewSample1Entities())
                {
                    currentUserTask = entity.usertasks.Where(w => w.Id == id).Select(
                      w => new UserTaskDto
                      {
                          Id = w.Id,
                          Description = w.Description,
                          Priorty = w.Priorty,
                          Status = w.Status,
                          Task = w.Task,
                          UserId = w.UserId

                      }).FirstOrDefault();
                }
            }
            catch
            {

            }

            return currentUserTask;
        }

        /// <summary>
        /// Tüm user task'leri getirir
        /// </summary>
        /// <returns></returns>
        public List<UserTaskDto> GetTasks()
        {

            var list = new List<UserTaskDto>();

            using (entity = new InterviewSample1Entities())
            {
                list = entity.usertasks.Select(
                    w => new UserTaskDto
                    {
                        Id = w.Id,
                        Description = w.Description,
                        Priorty = w.Priorty,
                        Status = w.Status,
                        Task = w.Task,
                        UserId = w.UserId

                    }).ToList();
            }

            return list;
        }

        /// <summary>
        /// Spesifik userId'li UserTask'i getirir
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<UserTaskDto> GetTasks(int userId)
        {

            var list = new List<UserTaskDto>();

            using (entity = new InterviewSample1Entities())
            {
                list = entity.usertasks.Where(w => w.UserId == userId).Select(
                    w => new UserTaskDto
                    {
                        Id = w.Id,
                        Description = w.Description,
                        Priorty = w.Priorty,
                        Status = w.Status,
                        Task = w.Task,
                        UserId = w.UserId

                    }).ToList();
            }

            return list;
        }

        /// <summary>
        /// Yani UserTask Kaydı yapar
        /// </summary>
        /// <param name="userTaskDto"></param>
        /// <returns></returns>
        public bool AddTask(UserTaskDto userTaskDto)
        {
            bool result = false;
            try
            {
                var newUserTask = new usertasks();

                MapperHelper.MapUserTask(userTaskDto, newUserTask);

                using (entity = new InterviewSample1Entities())
                {
                    entity.usertasks.Add(newUserTask);

                    result = entity.SaveChanges() > 0 ? true : false;
                }
            }
            catch
            {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// UserTask'i düzenler
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public bool EditTask(UserTaskDto userTaskDto)
        {
            bool result = false;
            try
            {
                using (entity = new InterviewSample1Entities())
                {
                    var currentUserTask = entity.usertasks.FirstOrDefault(w => w.Id == userTaskDto.Id);
                    if (currentUserTask == null)
                        return false;


                    userTaskDto.Id = currentUserTask.Id;
                    userTaskDto.UserId = currentUserTask.UserId;

                    MapperHelper.MapUserTask(userTaskDto, currentUserTask);

                    result = entity.SaveChanges() > 0 ? true : false;
                }
            }
            catch
            {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// UserTask'i siler
        /// </summary>
        /// <param name="userTaskDto"></param>
        /// <returns></returns>
        public bool DeleteTask(UserTaskDto userTaskDto)
        {
            bool result = false;
            try
            {
                using (entity = new InterviewSample1Entities())
                {
                    var currentUserTask = entity.usertasks.FirstOrDefault(w => w.Id == userTaskDto.Id);

                    if (currentUserTask == null)
                        return false;
                    else
                        entity.usertasks.Remove(currentUserTask);


                    userTaskDto.Id = currentUserTask.Id;
                    userTaskDto.UserId = currentUserTask.UserId;

                    result = entity.SaveChanges() > 0 ? true : false;
                }
            }
            catch
            {
                result = false;
            }

            return result;
        }



    }
}
