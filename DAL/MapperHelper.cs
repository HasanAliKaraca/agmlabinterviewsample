﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DOMAIN;
using DTO;

namespace DAL
{
    /*
     * Not: Normalde  map işlemi manuel yapılmaz, automapper kullanılması lazım ancak database first yaklaşımından
     * kullanıldığı için model1.tt dosyasının oluşturduğu classlar domain projesi içinde kalmıştır.
     * Proje mimarisi değiştirilmesi istenmediği için de automapper kullanamadım. 
     * Entity objectten dto'ya map işlemini bu yüzden ben de manuel gerçekleştirdim.
     *
     */

    public class MapperHelper
    {

        public static void MapUserTask(usertasks source, UserTaskDto destination)
        {
            destination.Description = source.Description;
            destination.Priorty = source.Priorty;
            destination.Status = source.Status;
            destination.Task = source.Task;
            destination.UserId = source.UserId;
        }

        public static void MapUserTask(UserTaskDto source, usertasks destination)
        {
            destination.Description = source.Description;
            destination.Priorty = source.Priorty;
            destination.Status = source.Status;
            destination.Task = source.Task;
            destination.UserId = source.UserId;
        }

    }
}
