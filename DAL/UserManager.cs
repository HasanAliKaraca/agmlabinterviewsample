﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DOMAIN;
using DTO;

namespace DAL
{
    public class UserManager : BaseManager
    {
        public List<UserDto> GetUsers()
        {
            List<UserDto> returnList = new List<UserDto>();

            using (InterviewSample1Entities entity = new InterviewSample1Entities())
            {
                returnList = entity.users.Select
                      (
                      x => new UserDto
                      {
                          Address = x.Address,
                          Email = x.Email,
                          Id = x.Id,
                          MobileNumber = x.MobileNumber,
                          Name = x.Name,
                          Surname = x.Surname
                      }).ToList();
            }

            return returnList;
        }

        public bool AddUser(UserDto dto)
        {
            bool returnBool = false;
            try
            {
                users newUser = new users
                {
                    Surname = dto.Surname,
                    Name = dto.Name,
                    MobileNumber = dto.MobileNumber,
                    Email = dto.Email,
                    Address = dto.Address,
                    Password = dto.Password
                };

                using (InterviewSample1Entities entity = new InterviewSample1Entities())
                {
                    entity.users.Add(newUser);

                    returnBool = entity.SaveChanges() > 0 ? true : false;
                }
            }
            catch
            {
                returnBool = false;
            }

            return returnBool;
        }

        public bool EditUser(UserDto dto)
        {
            bool returnBool = false;
            try
            {
                using (InterviewSample1Entities entity = new InterviewSample1Entities())
                {
                    users currentUser = entity.users.Where(x => x.Id == dto.Id).FirstOrDefault();
                    if (currentUser == null)
                        return false;

                    currentUser.Address = dto.Address;
                    currentUser.Email = dto.Email;
                    currentUser.MobileNumber = dto.MobileNumber;
                    currentUser.Name = dto.Name;
                    currentUser.Password = dto.Password;
                    currentUser.Surname = dto.Surname;

                    returnBool = entity.SaveChanges() > 0 ? true : false;
                }
            }
            catch
            {
                returnBool = false;
            }

            return returnBool;
        }

        public bool DeleteUser(int id)
        {
            bool returnBool = false;
            try
            {
                using (InterviewSample1Entities entity = new InterviewSample1Entities())
                {
                    users currentUser = entity.users.Where(x => x.Id == id).FirstOrDefault();
                    if (currentUser == null)
                        return false;
                    else
                        entity.users.Remove(currentUser);

                    returnBool = entity.SaveChanges() > 0 ? true : false;
                }
            }
            catch
            {
                returnBool = false;
            }

            return returnBool;
        }

        public UserDto GetUser(int id)
        {
            UserDto currentUser = new UserDto();
            try
            {
                using (InterviewSample1Entities entity = new InterviewSample1Entities())
                {
                    currentUser = entity.users.Where(x => x.Id == id).Select
                      (
                      x => new UserDto
                      {
                          Address = x.Address,
                          Email = x.Email,
                          Id = x.Id,
                          MobileNumber = x.MobileNumber,
                          Name = x.Name,
                          Surname = x.Surname
                      }).FirstOrDefault();
                }
            }
            catch
            {

            }

            return currentUser;
        }
    }
}
