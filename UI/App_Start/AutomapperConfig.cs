﻿using AutoMapper;
using DTO;
using UI.Models;
using UI.Models.TaskModels;

namespace UI
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            //entity model classları database first olduğundan, classlar model1.tt dosyası tarafından generate edilir.
            //ayrı bir proje ya da classda olmadığından automapper kullanamadım. 
            //Normal bir projede model.tt'nin oluşturduğu classlar ile DTO classları burada map edilmesi gerekir.
            //
            //ör: Mapper.CreateMap<users, UserDto>();

            #region view model - dto mapping

            Mapper.CreateMap<UserTaskDto, TaskViewModel>();
            Mapper.CreateMap<TaskViewModel, UserTaskDto>();

            #endregion
        }
    }
}