﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UI.Models.TaskModels
{
    public class TaskViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }

        [Display(Name = "Görev")]
        [Required(ErrorMessage = "Lütfen bir görev giriniz.")]
        public string Task { get; set; }


        [Display(Name = "Açıklama")]
        [Required(ErrorMessage = "Lütfen bir açıklama giriniz.")]
        public string Description { get; set; }


        [Display(Name = "Öncelik")]
        [Required(ErrorMessage = "Lütfen bir 0-255 arasında bir öncelik giriniz.")]
        [Range(0, 255)]
        public byte Priorty { get; set; }


        [Display(Name = "Durum")]
        [Required(ErrorMessage = "Lütfen durumu seçiniz.")]
        public bool Status { get; set; }
    }
}