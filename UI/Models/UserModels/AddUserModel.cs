﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class AddUserModel
    {
        public UserDto User { get; set; }

        public AddUserModel()
        {
            User = new UserDto();
        }
    }
}