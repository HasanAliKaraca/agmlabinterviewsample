﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class UserEditModel
    {
        public UserDto User { get; set; }

        public UserEditModel()
        {
            User = new UserDto();
        }
    }
}