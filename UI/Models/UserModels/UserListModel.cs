﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class UserListModel
    {
        public List<UserDto> Users { get; set; }

        public UserListModel()
        {
            Users = new List<UserDto>();
        }
    }
}