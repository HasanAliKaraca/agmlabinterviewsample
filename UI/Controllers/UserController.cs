﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UI.Models;

namespace UI.Controllers
{
    public class UserController : Controller
    {
        #region Index
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ListUsers
        public ActionResult UserList()
        {
            DAL.UserManager manager = new DAL.UserManager();
            var result = manager.GetUsers();

            UserListModel model = new UserListModel();
            model.Users = result;
            ViewBag.Submitted = false;
            return View(model);
        }
        #endregion

        #region Add User
        [HttpGet]
        public ActionResult AddUser()
        {
            AddUserModel model = new AddUserModel();
            ViewBag.Submitted = false;
            return View(model);
        }

        [HttpPost]
        public ActionResult AddUser(AddUserModel model)
        {
            DAL.UserManager manager = new DAL.UserManager();
            var result = manager.AddUser(model.User);

            ViewBag.Result = result;
            ViewBag.Submitted = true;
            return View();
        }
        #endregion

        #region Edit User
        [HttpGet]
        public ActionResult EditUser(int id)
        {
            DAL.UserManager manager = new DAL.UserManager();
            UserEditModel model = new UserEditModel();
            model.User = manager.GetUser(id);
            ViewBag.Submitted = false;
            return View(model);
        }
        [HttpPost]
        public ActionResult EditUser(UserEditModel model)
        {
            DAL.UserManager manager = new DAL.UserManager();
            var result = manager.EditUser(model.User);

            ViewBag.Result = result;
            ViewBag.Submitted = true;
            return View();
        }
        #endregion

        #region DeleteUser
        public ActionResult DeleteUser(int id)
        {
            DAL.UserManager manager = new DAL.UserManager();
            var result = manager.DeleteUser(id);

            ViewBag.Result = result;
            ViewBag.Submitted = true;
            return View();
        }
        #endregion
    }
}