﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DAL;
using DTO;
using UI.Models;
using UI.Models.TaskModels;

namespace UI.Controllers
{
    /*
     * Not: Normalde burada tanımlanan modeller viewmodel olarak tanımlanması gerekiyor. 
     * Ancak kod bütünlüğü sağlamak adına, aynı mimariyi devam ettirdim.
     * 
     */
    public class TaskController : Controller
    {
        private TaskManager _manager;
        private TaskViewModel _model;
        public TaskController()
        {
            _manager = new TaskManager();
            _model = new TaskViewModel();
        }


        #region Index
        public ActionResult Index()
        {
            var taskDtoList = _manager.GetTasks();
            var modelList = new List<TaskViewModel>();

            foreach (var item in taskDtoList)
            {
                var model = new TaskViewModel();

                Mapper.Map(item, model);
                modelList.Add(model);
            }

            ViewBag.Submitted = false;

            return View(modelList);
        }
        #endregion

        #region ListTasks
        public ActionResult TaskList(int userId)
        {
            ViewBag.UserId = userId;

            var taskDtoList = _manager.GetTasks(userId);
            var modelList = new List<TaskViewModel>();

            foreach (var item in taskDtoList)
            {
                var model = new TaskViewModel();

                Mapper.Map(item, model);
                modelList.Add(model);
            }

            ViewBag.Submitted = false;

            return View(modelList);
        }
        #endregion

        #region Add Task

        [HttpGet]
        public ActionResult AddTask(int userId)
        {
            var model = new TaskViewModel();
            model.UserId = userId;

            ViewBag.Submitted = false;

            return View(model);
        }


        [HttpPost]
        public ActionResult AddTask(TaskViewModel model)
        {
            var dto = new UserTaskDto();
            Mapper.Map(model, dto);

            var result = _manager.AddTask(dto);

            ViewBag.Result = result;

            ViewBag.Submitted = true;

            return View(model);
        }
        #endregion

        #region Edit Task

        [HttpGet]
        public ActionResult EditTask(int id)
        {

            var dto = _manager.GetTask(id);

            Mapper.Map(dto, _model);

            ViewBag.Submitted = false;

            return View(_model);
        }

        [HttpPost]
        public ActionResult EditTask(TaskViewModel model)
        {
            var dto = new UserTaskDto();

            Mapper.Map(model, dto);

            var result = _manager.EditTask(dto);

            model.UserId = dto.UserId;
            model.Id = dto.Id;

            ViewBag.Result = result;

            ViewBag.Submitted = true;

            return View(model);
        }
        #endregion

        #region Delete Task
        [HttpGet]
        public ActionResult DeleteTask(int id)
        {
            var dto = _manager.GetTask(id);

            Mapper.Map(dto, _model);

            ViewBag.Submitted = false;

            return View(_model);
        }

        [HttpPost]
        public ActionResult DeleteTask(TaskViewModel model)
        {
            var dto = new UserTaskDto();

            Mapper.Map(model, dto);

            var result = _manager.DeleteTask(dto);

            ViewBag.Result = result;
            ViewBag.Submitted = true;


            model.UserId = dto.UserId;
            model.Id = dto.Id;

            return View(model);
        }
        #endregion
    }
}