//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DOMAIN
{
    using System;
    using System.Collections.Generic;
    
    public partial class usertasks
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Description { get; set; }
        public string Task { get; set; }
        public byte Priorty { get; set; }
        public bool Status { get; set; }
    
        public virtual users users { get; set; }
    }
}
