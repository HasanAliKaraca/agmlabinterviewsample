﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class UserTaskDto : BaseDto
    {
        public int UserId { get; set; }
        public string Description { get; set; }
        public string Task { get; set; }
        public byte Priorty { get; set; }
        public bool Status { get; set; }

        
    }
}
